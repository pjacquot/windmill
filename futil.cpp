#include "futil.h"

using namespace std;

double readNumber(const string & line, unsigned int & index)
{
    string number;
    while(index < line.size() && line[index] != ',')
    {
          number += line[index];
          ++index;
    }
    index += 2; //skipping the white space;
    return stod(number);
}

string readString(const string &line, unsigned int &index)
{
    string word;
    while(index < line.size() && line[index] != ',')
    {
          word += line[index];
          ++index;
    }
    index += 2; //skipping the white space;
    return word;
}

pointStruct * readPoint(const string & line)
{
    pointStruct * p = new pointStruct;
    unsigned int i = 0;
    p->x = readNumber(line, i);
    p->y = readNumber(line, i);
    p->z = readNumber(line, i);
    return p;
}

rotationStruct * readRotation(const string & line)
{
    rotationStruct * rotation = new rotationStruct;

    unsigned int i = 0;
    rotation->angle = readNumber(line, i);
    rotation->x = readNumber(line, i);
    rotation->y = readNumber(line, i);
    rotation->z = readNumber(line, i);

    return rotation;
}

void readArray(const string & line, GLfloat * array)
{
    unsigned int index = 0;

    for(unsigned int i = 0; i < 4; ++i)
        array[i] = readNumber(line, index);
}

materialStruct * readMaterial(ifstream & stream)
{
    materialStruct * material = new materialStruct;
    string line;
    unsigned int index = 0;

    getline(stream, line);
    readArray(line, material->ambient);

    getline(stream, line);
    readArray(line, material->diffuse);

    getline(stream, line);
    readArray(line, material->specular);

    getline(stream, line);
    material->shininess = readNumber(line, index);

    return material;
}

colorStruct * readColor(const string & line)
{
    colorStruct * color = new colorStruct;
    unsigned int index = 0;

    color->red = readNumber(line, index);
    color->green = readNumber(line, index);
    color->blue = readNumber(line, index);
    color->alpha = readNumber(line, index);

    return color;
}
