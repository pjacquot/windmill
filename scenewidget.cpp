#include "scenewidget.h"

#include <GL/glu.h>

#include <fstream>
#include <math.h>

#include "objects/cylinderobject.h"
#include "objects/planeobject.h"
#include "objects/multiobject.h"
#include "objects/abstractobject.h"
#include "objects/jamyobject.h"

using namespace std;

SceneWidget::SceneWidget(QWidget * parent)
    : QGLWidget(parent), objects()
{
    axis = false;
    time = false;

    camera = new pointStruct{4.0, 2.0, 4.0};

    light = new GLfloat[4];
    light [0] = 0.0; light[1] = 10.; light[3] = 0.0; light[4] = 0.0;

    lightDir = new GLfloat[3];
    lightDir[0] = 0.0; lightDir[1] = -1.; lightDir[2] = 0.;

    t = 0;

    setupScene();
}

// == public slots ===

void SceneWidget::updateObjects()
{
    if(time)
    {
        rotateCamera(t);
        moveLight(t);
        moveIsland(t);
        // === objects movements ===
        pales->rotate(-1);

        ++t;
    }
}


void SceneWidget::rotateCamera(int angle)
{
    camera->x = 5.65*cos(0.01 * angle);
    camera->z = 5.65*sin(0.01 * angle);
}

void SceneWidget::rotateWindmill(int angle)
{
    windmill->setRotation(angle, 0.0, 1.0, 0.0);
}

void SceneWidget::rotatePales(int angle)
{
    pales->setRotation(angle, 1.0, 0.0, 0.0);
}

void SceneWidget::moveLight(int angle)
{
    light[2] = 5.0*sin(0.005*angle);
    light[1] = 5.0*cos(0.005*angle);
    lightDir[2] = -sin(0.005*angle);
    lightDir[1] = -cos(0.005*angle);
}

void SceneWidget::rotateIsland(int angle)
{
    island->setRotation(angle, 1.0, 0.0, 0.0);
}

void SceneWidget::moveIsland(int coeff)
{
    island->setTranslation(0.0, cos(0.1*coeff)/2, 0.0);
}
void SceneWidget::toggleAxis()
{
    axis = !axis;
}

void SceneWidget::toggleTime()
{
    time = !time;
}

// === protected methods ===

void SceneWidget::setLight() const
{
    glLightfv(GL_LIGHT0, GL_POSITION, light);
    glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, lightDir);
    glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, 30.);
}

void SceneWidget::setupScene()
{
    ifstream stream("res/island");
    AbstractObject * island = new MultiObject(stream);
    //objects.push_back(island);

    stream.close();
    stream.open("res/tree");
    AbstractObject * tree = new MultiObject(stream);
    tree->move(-1.0, 0.0, -1.0);
    //objects.push_back(tree);

    stream.close();
    stream.open("res/tree");
    AbstractObject * tree2 = new MultiObject(stream);
    tree2->move(0.5, 0.0, 0.5);
    //objects.push_back(tree2);

    stream.close();
    stream.open("res/tree");
    AbstractObject * tree3 = new MultiObject(stream);
    tree3->move(-1.75, 0.0, -0.75);
    //objects.push_back(tree3);

    stream.close();
    stream.open("res/tree");
    AbstractObject * tree4 = new MultiObject(stream);
    tree4->move(-1.0, 0.0, 0.8);
    //objects.push_back(tree4);

    stream.close();
    stream.open("res/tree");
    AbstractObject * tree5 = new MultiObject(stream);
    tree5->move(-2.0, 0.0, 1.0);
    //objects.push_back(tree5);

    // === Creating the multiObject for the windmill ===

    unsigned int nObjects = 2;
    AbstractObject ** objectsList = new AbstractObject * [nObjects];

    stream.close();
    stream.open("res/windmill");
    AbstractObject * house = new MultiObject(stream);
    //objects.push_back(house);

    objectsList[0] = house;
    // === creating the multiobject pales. ===

    AbstractObject ** tab = new AbstractObject * [4];
    for(unsigned int i = 0; i < 4; ++i)
    {
        stream.close();
        stream.open("res/pale");
        AbstractObject * pale = new PlaneObject(stream);
        pale->setRotation(i*90., 1.0, 0.0, 0.0);
        tab[i] = pale;
    }

    pales = new MultiObject(4, tab);
    pales->setTranslation(0.5, 2.0, 0.0);
    pales->setRotation(0.0, 1.0, 0.0, 0.0);
    //objects.push_back(pales);

    objectsList[1] = pales;
    // === end of pales ===
    windmill = new MultiObject(nObjects, objectsList);
    windmill->setRotation(0.0, 0.0, 1.0, 0.0);
    windmill->setTranslation(-2.0, 0.0, 0.0);
    //objects.push_back(windmill);

    // === end of windmill ===
    stream.close();
    stream.open("res/fence");
    PlaneObject * fence1 = new PlaneObject(stream);
    fence1->setTranslation(-0.4, 0.0, 0.95);
    fence1->setRotation(30., 0.0, 1.0, 0.0);
    //objects.push_back(fence1);

    stream.close();
    stream.open("res/fence");
    PlaneObject * fence2 = new PlaneObject(stream);
    fence2->setTranslation(-0.8, 0.0, -1.5);
    //objects.push_back(fence2);

    stream.close();
    stream.open("res/fence");
    PlaneObject * fence3 = new PlaneObject(stream);
    fence3->setTranslation(1.4, 0.0, 0.4);
    //objects.push_back(fence3);

    stream.close();
    stream.open("res/fence");
    PlaneObject * fence4 = new PlaneObject(stream);
    fence4->setTranslation(1.7, 0.0, -0.25);
    fence4->setRotation(-15., 0.0, 1.0, 0.0);
    //objects.push_back(fence4);

    AbstractObject * jamy = new JamyObject("res/Moi.ppm");
    jamy->setTranslation(3.5, 0.0, 0.0);

    unsigned int islandNumber = 12;
    AbstractObject ** islandObjects = new AbstractObject * [islandNumber];
    islandObjects[0] = island;
    islandObjects[1] = tree;
    islandObjects[2] = tree2;
    islandObjects[3] = tree3;
    islandObjects[4] = tree4;
    islandObjects[5] = tree5;
    islandObjects[6] = windmill;
    islandObjects[7] = fence1;
    islandObjects[8] = fence2;
    islandObjects[9] = fence3;
    islandObjects[10] = fence4;
    islandObjects[11] = jamy;

    // I use this->island so that there is no mistakes with
    // the multiobject * island above.
    // That's not very clean...
    this->island = new MultiObject(islandNumber, islandObjects);
    objects.push_back(this->island);
}

// === QGLWidget methods overriden ===

void SceneWidget::initializeGL()
{
    double coeff = cos(t*0.005);
    glClearColor(0.2 + 0.21*coeff, 0.2 + 0.61*coeff, 0.2 + 0.76*coeff, 0.);
}

void SceneWidget::resizeGL(int w, int h)
{
    glViewport(0, 0, w, h);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    setLight();

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    gluPerspective(60., 1.5, 1., 10.);
}

void SceneWidget::paintGL()
{
    // Clear the widget.
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);

    initializeGL();

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE);

    if(axis)
        drawAxis();

    setLight();

    for(auto object : objects)
    {
        object->render();
    }

    glLoadIdentity();

    gluLookAt(camera->x, camera->y, camera->z,
              0., 0., 0.,
              0., 1., 0.);

    glFlush();
}

// === special drawing methods ===

void SceneWidget::drawAxis() const
{
    glPushAttrib(GL_LIGHTING_BIT);
    glDisable(GL_LIGHT0);
    glDisable(GL_LIGHTING);
    glColor3f(1.0, 0.0, 0.0);
    glBegin(GL_LINE_STRIP);
        glVertex3f(0.0, 0.0, 0.0);
        glVertex3f(1.0, 0.0, 0.0);
    glEnd();


    glColor3f(0.0, 1.0, 0.0);
    glBegin(GL_LINE_STRIP);
        glVertex3f(0.0, 0.0, 0.0);
        glVertex3f(0.0, 1.0, 0.0);
    glEnd();

    glColor3f(0.0, 0.0, 1.0);
    glBegin(GL_LINE_STRIP);
        glVertex3f(0.0, 0.0, 0.0);
        glVertex3f(0.0, 0.0, 1.0);
    glEnd();
    glPopAttrib();
}
