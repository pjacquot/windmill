#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMenu>
#include <QMenuBar>
#include <QHBoxLayout>
#include <QSlider>
#include <QPushButton>

#include "scenewidget.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:

    SceneWidget * scene;

    QSlider * cameraSlider;
    QSlider * lightSlider;
    QSlider * windmillSlider;
    QSlider * palesSlider;
    QSlider * islandSlider;
};
#endif // MAINWINDOW_H
