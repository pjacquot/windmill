#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QTimer>
#include <QVBoxLayout>
#include <QLabel>

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
{
    this->setMinimumSize(1000, 800);

    scene = new SceneWidget(this);
    scene->setMinimumSize(800, 500);

    // Widgets on the right of the window

    QPushButton * timeButton = new QPushButton("Toggle time", this);
    QPushButton * axisButton = new QPushButton("toggleAxis", this);

    QHBoxLayout * buttonsLayout = new QHBoxLayout();
    buttonsLayout->addWidget(timeButton);
    buttonsLayout->addWidget(axisButton);

    cameraSlider = new QSlider(this);
    cameraSlider->setTickInterval(1);
    cameraSlider->setOrientation(Qt::Horizontal);
    cameraSlider->setMaximum(666);

    lightSlider = new QSlider(this);
    //lightSlider->setTickInterval(1);
    lightSlider->setOrientation(Qt::Horizontal);
    lightSlider->setMaximum(1000);

    windmillSlider = new QSlider(this);
    windmillSlider->setTickInterval(1);
    windmillSlider->setOrientation(Qt::Horizontal);
    windmillSlider->setMaximum(360);

    palesSlider = new QSlider(this);
    palesSlider->setTickInterval(1);
    palesSlider->setOrientation(Qt::Horizontal);
    palesSlider->setMaximum(360);

    islandSlider = new QSlider(this);
    islandSlider->setTickInterval(1);
    islandSlider->setOrientation(Qt::Horizontal);
    islandSlider->setMaximum(360);

    QVBoxLayout * controlPanel = new QVBoxLayout();
    controlPanel->addLayout(buttonsLayout);
    controlPanel->addWidget(new QLabel("camera position :", this));
    controlPanel->addWidget(cameraSlider);
    controlPanel->addWidget(new QLabel("Light position :", this));
    controlPanel->addWidget(lightSlider);
    controlPanel->addWidget(new QLabel("Island rotation :", this));
    controlPanel->addWidget(islandSlider);
    controlPanel->addWidget(new QLabel("windmill rotation :", this));
    controlPanel->addWidget(windmillSlider);
    controlPanel->addWidget(new QLabel("pales rotation :", this));
    controlPanel->addWidget(palesSlider);


    QHBoxLayout * globalLayout = new QHBoxLayout();

    globalLayout->addWidget(scene);
    globalLayout->addLayout(controlPanel);

    this->setLayout(globalLayout);

    QTimer * timer = new QTimer(this);
    timer->start(50);

    connect(timer, SIGNAL(timeout()), scene, SLOT(updateObjects()));
    connect(timer, SIGNAL(timeout()), scene, SLOT(updateGL()));

    connect(timeButton, SIGNAL(released()), scene, SLOT(toggleTime()));
    connect(axisButton, SIGNAL(released()), scene, SLOT(toggleAxis()));
    connect(cameraSlider, SIGNAL(valueChanged(int)), scene, SLOT(rotateCamera(int)));
    connect(lightSlider, SIGNAL(valueChanged(int)), scene, SLOT(moveLight(int)));
    connect(islandSlider, SIGNAL(valueChanged(int)), scene, SLOT(rotateIsland(int)));
    connect(windmillSlider, SIGNAL(valueChanged(int)), scene, SLOT(rotateWindmill(int)));
    connect(palesSlider, SIGNAL(valueChanged(int)), scene, SLOT(rotatePales(int)));

}

MainWindow::~MainWindow()
{

}

