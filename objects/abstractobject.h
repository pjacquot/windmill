#ifndef ABSTRACTOBJECT_H
#define ABSTRACTOBJECT_H

#include <fstream>

#include "geometry.h"

class AbstractObject
{
public:
// === Constructors and Destructor ===
    AbstractObject(pointStruct * off = nullptr, rotationStruct * rot = nullptr);
    AbstractObject(std::ifstream & stream);
    virtual ~AbstractObject();

// === public methods ===
    // display the object on the OpenGL scene.
    void render() const;
    void move(double x, double y, double z);
    void rotate(double da);

// === getters and setters ===
    void setTranslation(double x, double y, double z);
    void setRotation(double angle, double x, double y, double z);

protected:

// === Protected methods ===

    // read and set the offset and rotation values
    // from a stream.
    // Be aware of the file format you're using !
    void readSetupBase(std::ifstream & stream);

    // Perform the initial translation and rotation
    // before rendering the object.
    // Must be called before draw()
    void setupBase() const;
    
    // Draw the object by calling gl functions.
    virtual void draw() const = 0;

// === protected attributes ===

    // Origin point for relatives coordinates.
    pointStruct * offset;

    // Rotation to perform before rendering
    rotationStruct * rotation;
};

#endif // ABSTRACTOBJECT_H
