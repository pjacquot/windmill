#include "sceneobject.h"

#include <iostream>

#include "futil.h"

using namespace std;

SceneObject::SceneObject()
    : AbstractObject(), material(nullptr), color(nullptr)
{

}

SceneObject::SceneObject(
		    materialStruct * mat, 
		    colorStruct * col,
		    pointStruct * off
			)
    : AbstractObject(off), material(mat), color(col)
{

}

SceneObject::SceneObject(ifstream & stream)
    : AbstractObject(stream)
{
    readProperties(stream);
}

SceneObject::~SceneObject()
{
    delete material;
    delete color;
}

// === protected methods ===

void SceneObject::readProperties(ifstream &stream)
{
    string line;
// --- getting material ---
    material = readMaterial(stream);

// --- getting color ---
    getline(stream, line);
    color = readColor(line);
}

void SceneObject::setProperties() const
{
    if(material != nullptr)
    {
        glMaterialfv(GL_FRONT, GL_AMBIENT, material->ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE, material->diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR, material->specular);
        glMaterialf(GL_FRONT, GL_SHININESS, material->shininess);
    }
    else if(color != nullptr)
    {
        glColor3f(color->red, color->green, color->blue);
    }
}
