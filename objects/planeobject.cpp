#include "planeobject.h"

using namespace std;


// === Constructors and Destructor ===
PlaneObject::PlaneObject()
    : SceneObject(), nPlanes(0), planeList(nullptr)
{

}

PlaneObject::PlaneObject(
        materialStruct * mat,
        colorStruct * col,
        pointStruct * off,
        unsigned int n,
        Plane ** list
        )
    : SceneObject(mat, col, off), nPlanes(n), planeList(list)
{

}

PlaneObject::PlaneObject(ifstream & stream)
    : SceneObject(stream)
{
    readPlanes(stream);
}

PlaneObject::~PlaneObject()
{
    for(unsigned int i = 0; i < nPlanes; ++i)
        delete planeList[i];
    delete [] planeList;
}

// === Protected methods ===

void PlaneObject::readPlanes(ifstream & stream)
{
    string line;
    getline(stream, line);
    nPlanes = stoul(line.c_str());
    planeList = new Plane * [nPlanes];

    for(unsigned int i = 0; i < nPlanes; ++i)
    {
        planeList[i] = new Plane(stream);
    }
}

// === Redefined methods from SceneObject ===

void PlaneObject::draw() const
{
    setProperties();

    for(unsigned int i = 0; i < nPlanes; ++i)
    {
        planeList[i]->draw();
    }

}
