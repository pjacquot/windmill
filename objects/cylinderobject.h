#ifndef CYLINDEROBJECT_H
#define CYLINDEROBJECT_H

#include <fstream>

#include "sceneobject.h"

class CylinderObject : public SceneObject
{

public:

// === Constructors and Destructor ===
    CylinderObject();

    CylinderObject(
                materialStruct * mat,
                colorStruct * col,
                pointStruct * off,
                double baseR,
                double topR,
                double h,
                unsigned int slices,
                unsigned int stacks
            );

    CylinderObject(std::ifstream & stream);

    ~CylinderObject();



protected:

//=== Redefined methods from SceneObject ===
    void draw() const override;

// === protected attributes ===
    double baseRadius;
    double topRadius;
    double height;
    unsigned int slices;
    unsigned int stacks;
};

#endif // CYLINDEROBJECT_H
