#ifndef JAMYOBJECT_H
#define JAMYOBJECT_H

#include "sceneobject.h"
#include "Image.h"

class JamyObject : public SceneObject
{
public:
    JamyObject(const std::string & filename);

    // === method redefined from SceneObject ===
    void draw() const override;

protected:

    Image image;

};

#endif // JAMYOBJECT_H
