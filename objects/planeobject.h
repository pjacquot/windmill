#ifndef PLANEOBJECT_H
#define PLANEOBJECT_H

#include <fstream>

#include "sceneobject.h"
#include "plane.h"

class PlaneObject : public SceneObject
{
public:

// === Constructors and Destructor ===
    PlaneObject();

    PlaneObject(
        materialStruct * mat,
        colorStruct * col,
        pointStruct * off,
        unsigned int n,
        Plane ** list
        );

    PlaneObject(std::ifstream & stream);

    virtual ~PlaneObject();


protected:

// === Protected methods ===

    // Read and set the planes attributes
    // from a stream.
    // beware of the file format you're using !
    void readPlanes(std::ifstream & stream);

// === Redefined methods from SceneObject ===
    void draw() const;

// === Protected attributes ===
    unsigned int nPlanes;
    Plane ** planeList;
};

#endif // PLANEOBJECT_H
