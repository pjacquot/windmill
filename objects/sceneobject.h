#ifndef SCENEOBJECT_H
#define SCENEOBJECT_H

#include <GL/glu.h>
#include <fstream>
#include <string>

#include "abstractobject.h"



/*
 * A SceneObject is an Object that will be rendered in
 * the 3D scene. It has material properties, and a list
 * of planes that need to be rendered.
 */
class SceneObject : public AbstractObject
{

public:

// === Constructors and Destructor ===
    SceneObject();
    SceneObject(
		materialStruct * mat, 
		colorStruct * col,
		pointStruct * off
		);
    SceneObject(std::ifstream & stream);

    virtual ~SceneObject();

protected:

// === protected methods ===

    // read and set material and color values
    // from a stream.
    // Be aware of the file format you're using !
    void readProperties(std::ifstream & stream);

  /*
   * Set material, color properties in openGL.
   */
  void setProperties() const;

// === Methods to be redifined by child classes ===
  virtual void draw() const = 0;

// === Attributes declaration ===

  materialStruct * material;
  colorStruct * color;

};

#endif // SCENEOBJECT_H
