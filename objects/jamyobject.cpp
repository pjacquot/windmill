#include "jamyobject.h"



using namespace std;

JamyObject::JamyObject(const string & filename)
    : SceneObject(), image(filename.c_str())
{
    this->material = new materialStruct{
      { 1.0, 1.0, 1.0, 1.0},
      { 1.0, 1.0, 1.0, 1.0},
      { 1.0, 1.0, 1.0, 1.0},
      100.0
    };

}

void JamyObject::draw() const
{
    setProperties();
    glPushMatrix();
    glRotatef(-90., 1.0, 0.0, 0.0);
    GLUquadric * foot = gluNewQuadric();
    gluQuadricOrientation(foot, GLU_INSIDE);

    gluCylinder(
                foot,
                0.05,
                0.05,
                0.3,
                10,
                20
            );
    glPopMatrix();

    glEnable(GL_TEXTURE_2D);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image.Width(), image.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, image.imageField());

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    // cette face aura la texture.
    glBegin(GL_POLYGON);
        glNormal3f(1.0, 0.0, 0.0);

        glTexCoord2f(1.0, 1.0);
        glVertex3f(0.1, 0.5, -0.1);

        glTexCoord2f(1.0, 0.0);
        glVertex3f(0.1, 0.3, -0.1);

        glTexCoord2f(0.0, 0.0);
        glVertex3f(0.1, 0.3, 0.1);

        glTexCoord2f(0.0, 1.0);
        glVertex3f(0.1, 0.5, 0.1);

    glEnd();
    glBegin(GL_POLYGON);
    glNormal3f(-1.0, 0.0, 0.0);
        glVertex3f(-0.1, 0.3, 0.1);
        glVertex3f(-0.1, 0.3, -0.1);
        glVertex3f(-0.1, 0.5, -0.1);
        glVertex3f(-0.1, 0.5, 0.1);
    glEnd();

    glBegin(GL_POLYGON);
        glNormal3f(0.0, -1.0, 0.0);
        glVertex3f(0.1, 0.3, 0.1);
        glVertex3f(-0.1, 0.3, 0.1);
        glVertex3f(-0.1, 0.3, -0.1);
        glVertex3f(0.1, 0.3, -0.1);
    glEnd();

    glBegin(GL_POLYGON);
        glNormal3f(0.0, 1.0, 0.0);

        //glTexCoord2f(0.0, 0.0);
        glVertex3f(-0.1, 0.5, 0.1);

        //glTexCoord2f(1.0, 0.0);
        glVertex3f(-0.1, 0.5, -0.1);

        //glTexCoord2f(0.0, 1.0);
        glVertex3f(0.1, 0.5, -0.1);

        //glTexCoord2f(1.0, 1.0);
        glVertex3f(0.1, 0.5, 0.1);

    glEnd();

    glBegin(GL_POLYGON);
        glNormal3f(0.0, 0.0, 1.0);
        glVertex3f(0.1, 0.3, 0.1);
        glVertex3f(0.1, 0.5, 0.1);
        glVertex3f(-0.1, 0.5, 0.1);
        glVertex3f(-0.1, 0.3, 0.1);
    glEnd();
    glBegin(GL_POLYGON);
    glNormal3f(0.0, 0.0, -1.0);
        glVertex3f(0.1, 0.3, -0.1);
        glVertex3f(0.1, 0.5, -0.1);
        glVertex3f(-0.1, 0.5, -0.1);
        glVertex3f(-0.1, 0.3, -0.1);
    glEnd();

    glDisable(GL_TEXTURE_2D);
}
