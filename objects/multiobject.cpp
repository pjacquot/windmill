#include "multiobject.h"

#include <string>

#include "futil.h"
#include "planeobject.h"
#include "cylinderobject.h"

using namespace std;

// === Constructors and Destructor ===

MultiObject::MultiObject() : AbstractObject()
{

}

MultiObject::MultiObject(unsigned int n, AbstractObject ** list)
    : AbstractObject(), nObject(n), objectList(list)
{

}

MultiObject::MultiObject(ifstream & stream)
    : AbstractObject(stream)
{
    string line;

    // Reading the number of objects composing this one.
    getline(stream, line);
    nObject = stoul(line);

    objectList = new AbstractObject * [nObject];

    for(unsigned int i = 0; i < nObject; ++i)
    {
        getline(stream, line);
        if(line == "FILE")
        {
            getline(stream, line);
            objectList[i] = readObject(line);
        }
        else
            // Assuming that the object datas are
            // in the same file...
        {
            objectList[i] = readObject(stream);
        }
    }

}

MultiObject::~MultiObject()
{
    for(unsigned int i = 0; i < nObject; ++i)
        delete objectList[i];
    delete [] objectList;
}

// === Protected methods ===

AbstractObject * MultiObject::readObject(const string &line) const
// TODO : this method can be refactored.
{
    unsigned int index = 0;
    string filename = readString(line, index);
    string type = readString(line, index);

    ifstream objectFile(filename);

    return instanciateObject(objectFile, type);
}

AbstractObject * MultiObject::readObject(ifstream &stream) const
{
    string line;
    getline(stream, line);
    return instanciateObject(stream, line);
}

AbstractObject * MultiObject::instanciateObject(
        ifstream &stream,
        const string &type)
const
{
    AbstractObject * object = nullptr;

    if(type == "planeobject")
        object = new PlaneObject(stream);

    else if(type == "cylinderobject")
        object = new CylinderObject(stream);

    else if(type == "multiobject")
        object = new MultiObject(stream);

    return object;
}

// === Redefined methods from AbstractObject ===

void MultiObject::draw() const
{
    for(unsigned int i = 0; i < nObject; ++i)
    {
        objectList[i]->render();
    }
}
