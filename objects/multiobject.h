#ifndef MULTIOBJECT_H
#define MULTIOBJECT_H

#include <fstream>

#include "abstractobject.h"

class MultiObject : public AbstractObject
{
public:

// === Constructors and Destructor ===
    MultiObject();

    MultiObject(unsigned int nObject, AbstractObject ** objectList);

    MultiObject(std::ifstream & stream);

    ~MultiObject() override;



protected:

    // === protected methods ===

    // Read line and construct the adequate object
    // according to what is written in line.
    AbstractObject * readObject(const std::string & line) const;

    // Same as above, but directly in the file that has been
    // provided for the constructor.
    AbstractObject * readObject(std::ifstream & stream) const;

    // Just instianciate the right AbstractObject subclass
    // according to what word has been read into the file.
    AbstractObject * instanciateObject(
                                std::ifstream & stream,
                                const std::string & type
                            )
    const;

    // === redefined methods from AbstractObject ===
        void draw() const override;

    // === protected attributes ===

    unsigned int nObject;
    AbstractObject ** objectList;
};

#endif // MULTIOBJECT_H
