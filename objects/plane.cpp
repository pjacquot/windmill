#include "plane.h"
#include <GL/glu.h>

#include "futil.h"

using namespace std;

// === Constructors and Destructor ===
Plane::Plane(pointStruct ** array, unsigned int length, int mode)
    : pointsArray(array), arrayLength(length), mode(mode)
{
    computeNormal();
}

Plane::Plane(ifstream & stream)
{
    // reading the mode of rendering
    string line;
    getline(stream, line);
    mode = getMode(line);

    if(mode == GL_TRIANGLES)
        readAsTriangles(stream);
    else
        readAsList(stream);

    computeNormal();

}

Plane::Plane(pointStruct * a, pointStruct * b, pointStruct * c)
{
    mode = GL_TRIANGLES;
    arrayLength = 3;
    pointsArray = new pointStruct * [arrayLength];
    pointsArray[0] = a;
    pointsArray[1] = b;
    pointsArray[2] = c;

    computeNormal();
}

Plane::~Plane()
{
    for(unsigned int i = 0; i < arrayLength; ++i)
        delete pointsArray[i];
    delete [] pointsArray;
}

// === Public methods ===

void Plane::draw() const
{
    glNormal3f(normal->x, normal->y, normal->z);
    glBegin(mode);
    for(unsigned int i = 0; i < arrayLength; ++i)
    {
        pointStruct * p = pointsArray[i];
        glVertex3f(p->x,
                   p->y,
                   p->z);
    }
    glEnd();
}

GLenum Plane::getMode(const string & line)
{
    unsigned int mode;

    // TODO : add all types of rendering + refactor
    if(line == "GL_POLYGON")
        mode = GL_POLYGON;
    else if (line == "GL_TRIANGLES")
        mode = GL_TRIANGLES;
    else
        mode = 0;

    return mode;
}

// === Private Methods ===

void Plane::readAsList(ifstream & stream)
{
    string line;
    getline(stream, line);
    arrayLength = stoul(line);
    pointsArray = new pointStruct * [arrayLength];
    for(unsigned int i = 0; i < arrayLength; ++i)
    {
        getline(stream, line);
        pointsArray[i] = readPoint(line);
    }
}

void Plane::readAsTriangles(ifstream &stream)
{
    string line;
    unsigned int nPoints;
    unsigned int j = 0;

// === Number of points in the plane ===
    getline(stream, line);
    nPoints = stoul(line.c_str());
    arrayLength = (nPoints - 2)*3;
    pointsArray = new pointStruct * [arrayLength];

    // the values of a, b and c will be shifted.
    // <- a <- b <- c
    pointStruct * a = nullptr;

    getline(stream, line);
    pointStruct * b = readPoint(line);

    getline(stream, line);
    pointStruct * c = readPoint(line);

    for(unsigned int i = 2; i < nPoints; ++i)
    {
        getline(stream, line);
        a = deep_copy(b);
        b = deep_copy(c);
        c = readPoint(line);

        pointsArray[j++] = a;
        pointsArray[j++] = b;
        pointsArray[j++] = c;
    }
}

void Plane::computeNormal()
{
    normal = new pointStruct;
    // TODO : check if the plane has at least 3 points !!!
    pointStruct * a = pointsArray[0];
    pointStruct * b = pointsArray[1];
    pointStruct * c = pointsArray[2];
    pointStruct ab = { b->x - a->x, b->y - a->y, b->z - a->z};
    pointStruct ac = { c->x - a->x, c->y - a->y, c->z - a->z};
    //TODO : check if there is a vectorial product already made.

    normal->x = (ab.y)*(ac.z) - (ab.z)*(ac.y);
    normal->y = (ab.z)*(ac.x) - (ab.x)*(ac.z);
    normal->z = (ab.x)*(ac.y) - (ab.y)*(ac.x);
}
