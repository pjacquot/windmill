#include "cylinderobject.h"

#include "GL/glu.h"

using namespace std;


// === Constructors and Destructor ===
CylinderObject::CylinderObject()
    : SceneObject(),
      baseRadius(0.0),
      topRadius(0.0),
      height(0.0),
      slices(0),
      stacks(0)
{

}

CylinderObject::CylinderObject(
            materialStruct * mat,
            colorStruct * col,
            pointStruct * off,
            double baseR,
            double topR,
            double h,
            unsigned int slices,
            unsigned int stacks
        )
    : SceneObject(mat, col, off),
      baseRadius(baseR),
      topRadius(topR),
      height(h),
      slices(slices),
      stacks(stacks)
{}

CylinderObject::CylinderObject(ifstream & stream)
    : SceneObject(stream)
{
    string line;

    getline(stream, line);
    baseRadius = stod(line);

    getline(stream, line);
    topRadius = stod(line);

    getline(stream, line);
    height = stod(line);

    getline(stream, line);
    slices = stoul(line);

    getline(stream, line);
    stacks = stoul(line);

}

CylinderObject::~CylinderObject() {}

// === Redefined methods from SceneObject ===

void CylinderObject::draw() const
{
    setProperties();

    GLUquadric * cylinder = gluNewQuadric();
    gluQuadricOrientation(cylinder, GLU_INSIDE);

    gluCylinder(
                cylinder,
                baseRadius,
                topRadius,
                height,
                slices,
                stacks
            );
}
