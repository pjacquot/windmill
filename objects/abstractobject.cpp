#include "abstractobject.h"

#include <GL/glu.h>

#include "futil.h"
using namespace std;

// === Constructors and Destructor ===

AbstractObject::AbstractObject(pointStruct * off, rotationStruct * rot)
    : offset(off), rotation(rot)
{ }

AbstractObject::AbstractObject(ifstream & stream)
{
    readSetupBase(stream);
}

AbstractObject::~AbstractObject()
{
    // TODO : check if the struct are destroyed correctly.
    delete offset;
    delete rotation;
}

// === public methods ===

void AbstractObject::render() const
{
    glPushMatrix();

    setupBase();
    draw();

    glPopMatrix();
}

void AbstractObject::move(double x, double y, double z)
{
    offset->x += x;
    offset->y += y;
    offset->z += z;
}

void AbstractObject::rotate(double da)
{
    rotation->angle += da;
}

// === getters and setters ===

void AbstractObject::setTranslation(double x, double y, double z)
{
    if(offset == nullptr)
    {
	offset = new pointStruct;
    }
    offset->x = x;
    offset->y = y;
    offset->z = z;
}

void AbstractObject::setRotation(double angle, double x, double y, double z)
{
    if(rotation == nullptr)
    {
	rotation = new rotationStruct;
    }
    rotation->angle = angle;
    rotation->x = x;
    rotation->y = y;
    rotation->z = z;
}

// === Protected methods ===

void AbstractObject::readSetupBase(ifstream &stream)
{
    string line;

    // reading offset
    getline(stream, line);
    offset = readPoint(line);

    // reading rotation
    getline(stream, line);
    rotation = readRotation(line);
}

void AbstractObject::setupBase() const
{
    if(offset != nullptr)
	glTranslatef(offset->x, offset->y, offset->z);
    if(rotation != nullptr)
	glRotatef(rotation->angle, rotation->x, rotation->y, rotation->z);
}
