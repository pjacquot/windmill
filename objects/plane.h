#ifndef PLANE_H
#define PLANE_H

#include <string>
#include <fstream>

#include "geometry.h"
/*
 * Class Polygon encapsulate points location and
 * material prgit operties to be drawn more easily
 * by successive OpenGL calls.
 */

class Plane {

public:
// === Constructors and Destructor ===
    Plane(pointStruct ** array = nullptr, unsigned int length = 0, int mode = GL_POLYGON);
    Plane(std::ifstream & stream);
    Plane(pointStruct * a, pointStruct * b, pointStruct * c);

    ~Plane();

// === public methods ===
    void draw() const;

    // Returns the GLenum corresponding to its name
    // ( for ex. "GL_POLYGON" will return the value of
    // GL_POLYGON).
    static GLenum getMode(const std::string & line);

private:

// === private methods ===

    // Read points from stream and store them in
    // a way that they can be rendered as a list
    // (for GL_POLYGON or GL_STRIP_LINE...)
    void readAsList(std::ifstream & stream);

    // Read points from stream and store them in
    // a way that they can be rendered as GL_TRIANGLES
    void readAsTriangles(std::ifstream & stream);

    // Calculate normal of the plane with the three first
    // points composing it :
    // if the plane has (a, b, c, ...)
    // It will be the direct normal to ab x ac
    void computeNormal();

    // Theses coordinates are relatives coordinates.
    pointStruct ** pointsArray;
    unsigned int arrayLength;

    pointStruct * normal;

    unsigned int mode;
};
#endif
