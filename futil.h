#ifndef FUTIL_H
#define FUTIL_H

#include <string>
#include <fstream>

#include "geometry.h"

/*
 * In this file are declared functions for reading
 * informations into the objects files.
 * They are called (and should always be) in the
 * constructors of SceneObject and its subclasses.
 */

// TODO : make readNumber and readString more generic
// ( let specify token instead of a hard coded ','...)

// Read a number in a line that contains several numbers
// separated by a ',' character.
// You should call this function several times, with the same line
// and same index parameters in order to read the whole line.
double readNumber(const std::string & line, unsigned int & index);

// Read a string in a line that contains several string, each
// separated with a ',' character.
// You should call this function several times, with the same line
// and same index parameters in order to read the whole line.
std::string readString(const std::string & line, unsigned int & index);

// Read a point according to the following "format" :
// x coord', y coord', z coord',
// in the line given in parameter.
pointStruct * readPoint(const std::string & line);

// Read a rotation according to the following "format" :
// angle, x axis, y axis, z axis,
// in the line given in parameter.
rotationStruct * readRotation(const std::string & line);

// readArray read GLfloats on a same line and store them into
// the array.
// WARNING : the array must be at least of size 4 !
// If not, it will segfault !!!
void readArray(const std::string &line, GLfloat * array );

materialStruct * readMaterial(std::ifstream & stream);

colorStruct * readColor(const std::string & line);



#endif // FUTIL_H
