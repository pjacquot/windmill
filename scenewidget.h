#ifndef SCENEWIDGET_H
#define SCENEWIDGET_H

#include <QtOpenGL/QGLWidget>

#include <vector>

#include "objects/abstractobject.h"

class SceneWidget : public QGLWidget
{
    Q_OBJECT

public:
    SceneWidget(QWidget * parent);

public slots:

    // update camera and objects positions
    // and orientation.
    void updateObjects();

    void rotateCamera(int angle);
    void rotateWindmill(int angle);
    void rotatePales(int angle);
    void moveLight(int angle);

    void rotateIsland(int angle);
    void moveIsland(int coeff);

    void toggleAxis();
    void toggleTime();

protected:

    void setLight() const;

    // Setup the scene, by creating objects
    // from files in res/ and moving them
    // to their location.
    void setupScene();

    // === QGLWidget methods overriden ===
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

    // === special drawing methods ===
    void drawAxis() const;

private:
    std::vector <AbstractObject *> objects;

    // objects that need to be moved.
    AbstractObject * island;
    AbstractObject * windmill;
    AbstractObject * pales;

    pointStruct * camera;
    GLfloat * light;
    GLfloat * lightDir;

    boolean axis;
    boolean time;

    //counter used for movement.
    unsigned int t;
};

#endif // SCENEWIDGET_H
