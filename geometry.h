#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <GL/glu.h>

/*
 * Definitions of geometry structures
 * such as point, Rotations etc...
 */

/*
 * Data representation of 3D points.
 */
// TODO : Go from double to GLFloat.
typedef struct {
    double x;
    double y;
    double z;
} pointStruct;

pointStruct * deep_copy(pointStruct * point);
/*
 * Data representation for a rotation.
 * Rotation of angle, around (x,y,z) axis
 */
typedef struct 
{
    double angle;
    double x;
    double y;
    double z;
} rotationStruct;


/*
 * Data structure for material properties of the Object.
 */
typedef struct {
    GLfloat ambient[4];
    GLfloat diffuse[4];
    GLfloat specular[4];
    GLfloat shininess;
} materialStruct;


/*
 * Data structure for color properties of the Object.
 */
typedef struct 
{
    GLfloat red;
    GLfloat green;
    GLfloat blue;
    GLfloat alpha;
} colorStruct;

#endif
