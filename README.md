Pierre Jacquot (student ID : 201376063)
# Windmill

Coursework 2 of Computer Graphics.
Done with Qt and OpenGL.

This is a modeling of Noodle's windmill, from Gorillaz video clip.
You can find my inspiration source here : [El Manana](https://www.youtube.com/watch?v=hji4gBuOvIQ)

# Making the project run :

Depending how you launch the project, you may have to copy the `res` folder into your build folder. It contains the data files needed to create the scene.
